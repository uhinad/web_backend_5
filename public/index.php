<?php
  /**
   * Реализовать возможность входа с паролем и логином с использованием
   * сессии для изменения отправленных данных в предыдущей задаче,
   * пароль и логин генерируются автоматически при первоначальной отправке формы.
   */

  // Отправляем браузеру правильную кодировку,
  // файл index.php должен быть в кодировке UTF-8 без BOM.
  header('Content-Type: text/html; charset=UTF-8');

  // В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
  // и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
  if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();

    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('save', '', 100000);
      setcookie('login', '', 100000);
      setcookie('pass', '', 100000);
      // Выводим сообщение пользователю.
      $messages[] = 'Спасибо, результаты сохранены.';
      // Если в куках есть пароль, то выводим сообщение.
      if (!empty($_COOKIE['pass'])) {
        $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
          и паролем <strong>%s</strong> для изменения данных.',
          strip_tags($_COOKIE['login']),
          strip_tags($_COOKIE['pass']));
      }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['e_mail'] = !empty($_COOKIE['e_mail_error']);
    $errors['year'] = !empty($_COOKIE['year_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if (($errors['fio'])) {
      // Удаляем куку, указывая время устаревания в прошлом.
      setcookie('fio_error', '', 100000);
      // Выводим сообщение.
      $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if($errors['e_mail']){
      setcookie('e_mail_error', '', 100000);
      $messages[] = '<div>Заполните почту корректно.</div>';
    }
    if($errors['year']){
      setcookie('year_error', '', 100000);
      $messages[] = '<div>Заполните год корректно.</div>';
    }
    if($errors['biography']){
      setcookie('biography_error', '', 100000);
      $messages[] = '<div>Заполните биографию.</div>';
    }
    // TODO: тут выдать сообщения об ошибках в других полях.

    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['e_mail'] = empty($_COOKIE['e_mail_value']) ? '' : $_COOKIE['e_mail_value'];
    $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['super_skill'] = empty($_COOKIE['super_skill_value']) ? '' : $_COOKIE['super_skill_value'];
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    // TODO: аналогично все поля.

    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
      // TODO: загрузить данные пользователя из БД
      // и заполнить переменную $values,
      // предварительно санитизовав.
      printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода 
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
  }
  // Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
  else {
    // Проверяем ошибки.
    $errors = FALSE;

    setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    setcookie('super_skill_value', $_POST['super_skill'], time() + 30 * 24 * 60 * 60);

    if (empty($_POST['fio'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('fio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    
    if (empty($_POST['e_mail'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('e_mail_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('e_mail_value', $_POST['e_mail'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['year'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('year_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
    }

    if (empty($_POST['biography'])) {
      // Выдаем куку на день с флажком об ошибке в поле fio.
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }


    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************

    if ($errors) {
      // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
      header('Location: index.php');
      exit();
    }
    else {
      // Удаляем Cookies с признаками ошибок.
      setcookie('fio_error', '', 100000);
      setcookie('e_mail_error', '', 100000);
      setcookie('year_error', '', 100000);
      setcookie('biography_error', '', 100000);
      // TODO: тут необходимо удалить остальные Cookies.
    }

    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
      // TODO: перезаписать данные в БД новыми данными,
      // кроме логина и пароля.
    }
    else {
      // Генерируем уникальный логин и пароль.
      // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
      $login = '123';
      $pass = '123';
      // Сохраняем в Cookies.
      setcookie('login', $login);
      setcookie('pass', $pass);

      // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
      // ...

      $fio = $_POST['fio'];
      $e_mail = $_POST['e_mail'];
      $year = $_POST['year'];
      $gender = $_POST['gender'];
      $limbs = $_POST['limbs'];
      $biography = $_POST['biography'];
      $ability_god = '0';
      $ability_fly = '0';
      $ability_fireball = '0';
      $ability = $_POST['super_skill'];

      $user = 'u25580';
      $pass = '3457355';
      $table1_name ="application";
      $table2_name ="superpowers";
      $db = new PDO('mysql:host=localhost;dbname=u25580', $user, $pass);

      // Подготовленный запрос. Не именованные метки.
      try {
        $db->exec("set names utf8");
        $data1 = array('name1' => $fio, 'e_mail'=> $e_mail, 'year'=>$year, 'gender'=>$gender, 'limbs'=>$limbs, 'biography'=>$biography);
        $data2 = array('ability'=>$ability);
        $q1 = $db->prepare("INSERT INTO $table1_name (name1, e_mail, year, gender, limbs, biography) VALUES(:name1, :e_mail, :year, :gender, :limbs, :biography)");
        $q1->execute($data1);
        $q2 = $db->prepare("INSERT INTO $table2_name (ability) VALUES(:ability)");
        $q2->execute($data2);
      }
      catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
      }
    }

    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: index.php');
  }
?>
